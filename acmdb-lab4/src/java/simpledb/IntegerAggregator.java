package simpledb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;


    private Op what;
    private int gbfield;
    private Type gbfieldtype;
    private int afiedld;
    private HashMap<Field, Integer> tuplegroup;
    private HashMap<Field, Integer> count;
    private String gbfield_name;
    private String afield_name;


    /**
     * Aggregate constructor
     * 
     * @param gbfield
     *            the 0-based index of the group-by field in the tuple, or
     *            NO_GROUPING if there is no grouping
     * @param gbfieldtype
     *            the type of the group by field (e.g., Type.INT_TYPE), or null
     *            if there is no grouping
     * @param afield
     *            the 0-based index of the aggregate field in the tuple
     * @param what
     *            the aggregation operator
     */

    public IntegerAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        // some code goes here
        this.gbfield = gbfield;
        this.afiedld = afield;
        this.gbfieldtype = gbfieldtype;
        this.what = what;
        this.tuplegroup = new HashMap<Field, Integer>();
        this.count = new HashMap<Field, Integer>();
        this.gbfield_name = null;
        this.afield_name = null;
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the
     * constructor
     * 
     * @param tup
     *            the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // some code goes here
        if(this.gbfield_name == null || this.afield_name == null) {
            if(this.gbfield != Aggregator.NO_GROUPING) {
                this.gbfield_name = tup.getTupleDesc().getFieldName(this.gbfield);
            }
            this.afield_name = this.what + "(" + tup.getTupleDesc().getFieldName(this.afiedld) + ")";
        }
        Field key;
        if(this.gbfield == Aggregator.NO_GROUPING) {
            key = null;
        } else {
            key = tup.getField(this.gbfield);
        }

        int new_value = ((IntField) tup.getField(this.afiedld)).getValue();
        switch(this.what) {
            case MIN:
                if(this.tuplegroup.containsKey(key)) {
                    int old_value = this.tuplegroup.get(key);
                    if (new_value < old_value) {
                        this.tuplegroup.put(key, new_value);
                    }
                } else {
                    this.tuplegroup.put(key, new_value);
                }
                break;
            case MAX:
                if(this.tuplegroup.containsKey(key)) {
                    int old_value = this.tuplegroup.get(key);
                    if (new_value > old_value) {
                        this.tuplegroup.put(key, new_value);
                    }
                } else {
                    this.tuplegroup.put(key, new_value);
                }
                break;
            case SUM:
                if(this.tuplegroup.containsKey(key)) {
                    int old_value = this.tuplegroup.get(key);
                    this.tuplegroup.put(key, new_value + old_value);
                } else {
                    this.tuplegroup.put(key,new_value);
                }
                break;
            case AVG:
                if(this.tuplegroup.containsKey(key)) {
                    int old_value = this.tuplegroup.get(key);
                    int old_count = this.count.get(key);
                    this.tuplegroup.put(key, new_value + old_value);
                    this.count.put(key, old_count + 1);
                } else {
                    this.tuplegroup.put(key, new_value);
                    this.count.put(key, 1);
                }
                break;
            case COUNT:
                if(this.tuplegroup.containsKey(key)) {
                    int old_count = this.tuplegroup.get(key);
                    this.tuplegroup.put(key, old_count + 1);
                } else {
                    this.tuplegroup.put(key, 1);
                }
                break;
        }
    }

    /**
     * Create a DbIterator over group aggregate results.
     * 
     * @return a DbIterator whose tuples are the pair (groupVal, aggregateVal)
     *         if using group, or a single (aggregateVal) if no grouping. The
     *         aggregateVal is determined by the type of aggregate specified in
     *         the constructor.
     */
    public DbIterator iterator() {
        // some code goes here
        ArrayList<Tuple> tuples = new ArrayList<Tuple>();
        Iterator<Field> keys = this.tuplegroup.keySet().iterator();
        TupleDesc td;
        if(this.gbfield == Aggregator.NO_GROUPING) {
            Type[] types = { Type.INT_TYPE };
            String[] field_ar = {this.afield_name};
            td = new TupleDesc(types, field_ar);
        } else {
            Type[] types = { this.gbfieldtype, Type.INT_TYPE };
            String[] field_ar = {this.gbfield_name, this.afield_name};
            td = new TupleDesc(types, field_ar);
        }
        while(keys.hasNext()) {
            Field key = keys.next();
            IntField ans = null;
            switch(this.what) {
                case MIN:
                    ans = new IntField(this.tuplegroup.get(key));
                    break;
                case MAX:
                    ans = new IntField(this.tuplegroup.get(key));
                    break;
                case SUM:
                    ans = new IntField(this.tuplegroup.get(key));
                    break;
                case AVG:
                    int value = this.tuplegroup.get(key) / this.count.get(key);
                    ans = new IntField(value);
                    break;
                case COUNT:
                    ans = new IntField(this.tuplegroup.get(key));
                    break;
            }

            Tuple result = new Tuple(td);
            if(this.gbfield == Aggregator.NO_GROUPING) {
                result.setField(0, ans);
            } else {
                result.setField(0, key);
                result.setField(1, ans);
            }
            tuples.add(result);
        }

        if(tuples.size() == 0 && this.gbfield == Aggregator.NO_GROUPING) {
            Tuple result = new Tuple(td);
            IntField ans = null;
            if(this.what == Op.COUNT) {
                ans = new IntField(0);
            } else {
                ans = null;
            }
            result.setField(0, ans);
            tuples.add(result);
        }
        return new TupleIterator(td, tuples);
    }

}
