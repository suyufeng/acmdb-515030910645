package simpledb;

/** A class to represent a fixed-width histogram over a single integer-based field.
 */
public class IntHistogram {
    private int min;
    private int max;
    private int bucket_size;
    private int[] histograms;
    private int numTuple;
    /**
     * Create a new IntHistogram.
     *
     * This IntHistogram should maintain a histogram of integer values that it receives.
     * It should split the histogram into "buckets" buckets.
     *
     * The values that are being histogrammed will be provided one-at-a-time through the "addValue()" function.
     *
     * Your implementation should use space and have execution time that are both
     * constant with respect to the number of values being histogrammed.  For example, you shouldn't
     * simply store every value that you see in a sorted list.
     *
     * @param buckets The number of buckets to split the input value into.
     * @param min The minimum integer value that will ever be passed to this class for histogramming
     * @param max The maximum integer value that will ever be passed to this class for histogramming
     */
    public IntHistogram(int buckets, int min, int max) {
    	// some code goes here
        this.min = min;
        this.max = max;
        histograms = new int [buckets];
        numTuple = 0;
        bucket_size = (int) Math.ceil((max - min + 1) / (double) buckets);
    }

    /**
     * Add a value to the set of values that you are keeping a histogram of.
     * @param v Value to add to the histogram
     */
    public void addValue(int v) {
    	// some code goes here
        histograms[(v - min) / bucket_size]++;
        numTuple++;
    }

    /**
     * Estimate the selectivity of a particular predicate and operand on this table.
     *
     * For example, if "op" is "GREATER_THAN" and "v" is 5,
     * return your estimate of the fraction of elements that are greater than 5.
     *
     * @param op Operator
     * @param v Value
     * @return Predicted selectivity of this particular operator and value
     */

    private double get_equal(int bucket_id, int v) {
        return histograms[bucket_id] / (double)numTuple / (double)bucket_size;
    }

    private double get_great(int bucket_id, int v) {
        double base = ((bucket_size * (bucket_id + 1)) - 1 + min - v) * get_equal(bucket_id, v);
        for(int i = bucket_id + 1; i < histograms.length; i++) {
            base += histograms[i] / (double)numTuple;
        }
        return base;
    }
    private double get_less(int bucket_id, int v) {
        double base = (v - ((bucket_size * bucket_id) + min)) * get_equal(bucket_id, v);
        for(int i = bucket_id - 1; i >= 0; i--) {
            base += histograms[i] / (double)numTuple;
        }
        return base;
    }


    public double estimateSelectivity(Predicate.Op op, int v) {

    	// some code goes here
        int bucket_id = (v - min) / bucket_size;
        switch(op) {
            case EQUALS:
                if(v < min || v > max) {
                    return 0;
                } else {
                    return get_equal(bucket_id, v);
                }
            case NOT_EQUALS:
                if(v < min || v > max) {
                    return 1.0;
                }
                return 1.0 - get_equal(bucket_id, v);
            case GREATER_THAN:
                if(v >= max) {
                    return 0;
                } else if(v < min) {
                    return 1;
                } else {
                    return get_great(bucket_id, v);
                }
            case GREATER_THAN_OR_EQ:
                if(v > max) {
                    return 0;
                } else if (v < min) {
                    return 1;
                } else {
                    return get_equal(bucket_id, v) + get_great(bucket_id, v);
                }
            case LESS_THAN:
                if(v > max) {
                    return 1;
                } else if(v <= min) {
                    return 0;
                } else {
                    return get_less(bucket_id, v);
                }
            case LESS_THAN_OR_EQ:
                if(v > max) {
                    return 1;
                } else if (v < min) {
                    return 0;
                } else {
                    return get_equal(bucket_id, v) + get_less(bucket_id, v);
                }
        }
        return -1.0;
    }

    /**
     * @return
     *     the average selectivity of this histogram.
     *
     *     This is not an indispensable method to implement the basic
     *     join optimization. It may be needed if you want to
     *     implement a more efficient optimization
     * */
    public double avgSelectivity()
    {
        // some code goes here
        return 1.0;
    }

    /**
     * @return A string describing this histogram, for debugging purposes
     */
    public String toString() {
        // some code goes here
        return null;
    }
}
