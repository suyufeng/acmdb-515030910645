package simpledb;

import java.io.*;
import java.util.*;
/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {

    TupleDesc tupledesc;
    File file;
    int numPage;

    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
    public HeapFile(File f, TupleDesc td) {
        // some code goes here
        file = f;
        numPage = (int)(file.length() / BufferPool.getPageSize());
        tupledesc = td;
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        // some code goes here
        return file;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        // some code goes here
        return file.getAbsoluteFile().hashCode();
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
        return tupledesc;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
        // some code goes here
        RandomAccessFile random_file;
        HeapPage page = null;
        byte[] data = new byte[BufferPool.getPageSize()];
        try {
            random_file = new RandomAccessFile(file, "r");
            random_file.seek(pid.pageNumber() * BufferPool.getPageSize());
            random_file.read(data);
            random_file.close();
            page = new HeapPage((HeapPageId)pid, data);
        }  catch(IOException e) {
            e.printStackTrace();
        }
        return page;
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        // some code goes here
        // not necessary for lab1
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        // some code goes here
        return numPage;
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        return null;
        // not necessary for lab1
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
        // some code goes here
        return null;
        // not necessary for lab1
    }

    // see DbFile.java for javadocs
    public DbFileIterator iterator(TransactionId tid) {
        // some code goes here
        return new HeapFileIterator(tid);
    }

    private class HeapFileIterator implements DbFileIterator {
        TransactionId id;
        Iterator<Tuple> iterator = null;
        int pageid;
        boolean flag;

        public HeapFileIterator(TransactionId id) {
            this.id = id;
            pageid = 0;
            flag = false;
        }

        @Override
        public void open() throws DbException, TransactionAbortedException {
            flag = true;
            HeapPageId pid = new HeapPageId(getId(), pageid);
            HeapPage page = (HeapPage)((Database.getBufferPool()).getPage(id, pid, Permissions.READ_ONLY));
            iterator = page.iterator();
        }

        @Override
        public boolean hasNext() throws DbException, TransactionAbortedException {
            if(flag) {
                if(iterator == null) {
                    return false;
                }
                if(iterator.hasNext()) {
                    return true;
                }

                while(pageid < numPages() - 1) {
                    pageid++;
                    HeapPageId pid = new HeapPageId(getId(), pageid);
                    HeapPage page = (HeapPage)((Database.getBufferPool()).getPage(id, pid, Permissions.READ_ONLY));
                    iterator = page.iterator();
                    if(iterator.hasNext()) {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }

        @Override
        public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
            if(flag) {
                if(hasNext()) {
                    return iterator.next();
                }
            }
            throw new NoSuchElementException();
        }

        @Override
        public void rewind() throws DbException, TransactionAbortedException {
            close();
            open();
        }

        @Override
        public void close() {
            pageid = 0;
            flag = false;
            iterator = null;
        }
    }
}

